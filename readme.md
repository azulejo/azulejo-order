> Azulejo-order service

## Use

### Embedded module

```js
const azulejoOrder = require('@azulejo/order');

// middleware/index.js
module.exports = function (app) {
  app.use('/api/orders', azulejoOrder)
}
```

### Standalone app
```bash
npm run defconfig
PORT=<number> … npm start
```

## Migrations

Module configuration contains `schemas` option which points out the migrations's directory.
Do next steps to proceed:
1. Go to application directory where `@azulejo/order` was installed;
2. Add `schemas` option to application [configuration](./config/default.json#L5) unless exist;
3. Run migrations from application directory:
```bash
NODE_CONFIG_DIR="$(pwd)/config" npx knex --knexfile node_modules/@azulejo/order/knexfile.js migrate:latest
```

## Links
- [@featherjs generator](https://github.com/feathersjs/feathers#getting-started)
