// Update with your config settings.
const app = require('./index');

const { client, connection } = app.get('mysql');

module.exports = {

  development: {
    client,
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  production: {
    client,
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

};
