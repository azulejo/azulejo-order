const assert = require('assert');
const app = require('../../src/app');

describe('\'ordersOrderData\' service', () => {
  it('registered the service', () => {
    const service = app.service('orders-order-data');

    assert.ok(service, 'Registered the service');
  });
});
