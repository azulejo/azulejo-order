const assert = require('assert');
const app = require('../../src/app');

describe('\'order-active\' service', () => {
  it('registered the service', () => {
    const service = app.service('order-active');

    assert.ok(service, 'Registered the service');
  });
});
