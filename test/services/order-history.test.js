const assert = require('assert');
const app = require('../../src/app');

describe('\'orderHistory\' service', () => {
  it('registered the service', () => {
    const service = app.service('orderhistory');

    assert.ok(service, 'Registered the service');
  });
});
