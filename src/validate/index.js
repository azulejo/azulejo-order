const Ajv = require('ajv');
const debug = require('debug')('validate:index');

const ajv = new Ajv({ coerceTypes: true, v5: true });

module.constructor.prototype.import = function moduleImport (path) {
  return Promise.resolve().then(() => this.require(path));
};

exports.validator = ajv;

exports.addSchemas = async (app, file = 'order.schema') => {
  const path = require.resolve(`${app.get('schemas')}/${file}`);

  debug('Resolving schema', path);
  const schemas = await module.import(path);

  Object.keys(schemas).forEach((schema) => {
    debug(`Added \`${schema}\`.`, schemas[schema]);
    ajv.addSchema(schemas[schema], schemas[schema].title);
  });
};

exports.validate = (data, ...schemas) => Promise
  .all(schemas.map(it => ajv.validate({ $ref: it }, data)));

