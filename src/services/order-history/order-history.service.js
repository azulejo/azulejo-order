// Initializes the `orderHistory` service on path `/orderhistory`
const createService = require('feathers-knex');
const createModel = require('../../models/order-history.model');
const hooks = require('./order-history.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: createModel.tableName,
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/orderhistory', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('orderhistory');

  service.hooks(hooks);
};
