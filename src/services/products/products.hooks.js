/* eslint-disable no-multiple-empty-lines */


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      (context) => {
        if (context.params.transaction
          && context.params.transaction.trx) context.params.transaction.trx.commit();

        return context;
      },
    ],
    update: [
      (context) => {
        if (context.params.transaction
          && context.params.transaction.trx) context.params.transaction.trx.commit();

        return context;
      },
    ],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
