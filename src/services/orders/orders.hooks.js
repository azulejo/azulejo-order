/* eslint-disable no-multiple-empty-lines */


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      (context) => {
        const _data = context.data;

        if (!_data.history_id) _data.history_id = 1;
        if (!_data.status_status_id) _data.status_status_id = 1;
        if (!_data.number_id) _data.number_id = Math.round(new Date().getTime() / 1e3);

        context.data = _data; // eslint-disable-line no-param-reassign

        return context;
      },
    ],
    update: [
      (context) => {
        // eslint-disable-next-line no-param-reassign
        if (!context.data.editedon) context.data.editedon = new Date();

        throw new Error('Not implemented');
      },
    ],
    patch: [
      (context) => {
        // eslint-disable-next-line no-param-reassign
        if (!context.data.editedon) context.data.editedon = new Date();

        return context.service.get(context.id, context.params)
          .then((order) => {
            const historyParams = order;

            historyParams.order_order_id = order.id;

            delete historyParams.id;

            return context.app.service('/orderhistory').create(historyParams, context.params);
          })
          .then((history) => {
            context.data.history_id = history.history_id + 1; // eslint-disable-line no-param-reassign, max-len
            // bump history_id after history record was created

            return context;
          });
      },
    ],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [
      (context) => {
        context.params.transaction.trx.commit();

        return context;
      },
    ],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
