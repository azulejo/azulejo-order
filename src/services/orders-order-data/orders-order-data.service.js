// Initializes the `orders-order-data` service on path `/orders-order-data`
const createService = require('feathers-knex');
const createModel = require('../../models/orders-order-data.model');
const hooks = require('./orders-order-data.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: createModel.tableName,
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/orders-order-data', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('orders-order-data');

  service.hooks(hooks);
};
