/* eslint-disable no-multiple-empty-lines */


const tableName = (serviceName, app) => (typeof serviceName === 'string' ? app.service(serviceName) : serviceName).fullName;

module.exports = {
  before: {
    all: [],
    find: [
      (context) => {
        const { app, params } = context;
        const query = context.service.createQuery(params);

        query.innerJoin({ orderProducts: tableName('products', app) }, function innerJoin () {
          this.on(`${tableName(context.service)}.id`, '=', 'orderProducts.order_order_id');
        });

        query.select([
          'orderProducts.id',
          'orderProducts.product_id',
          'orderProducts.quantity',
          'orderProducts.price',
          'orderProducts.price_old',
        ]);

        context.params.knex = query; // eslint-disable-line no-param-reassign

        return context;
      },
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
