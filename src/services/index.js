const products = require('./products/products.service.js');

const orders = require('./orders/orders.service.js');

const orderHistory = require('./order-history/order-history.service.js');

const ordersOrderData = require('./orders-order-data/orders-order-data.service.js');

const orderActive = require('./order-active/order-active.service.js');

module.exports = function () {
  const app = this;

  app.configure(products);
  app.configure(orders);
  app.configure(orderHistory);
  app.configure(ordersOrderData);
  app.configure(orderActive);
};
