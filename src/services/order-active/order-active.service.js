// Initializes the `order-active` service on path `/order-active`
const createService = require('feathers-knex');
const createModel = require('../../models/order-active.model');
const hooks = require('./order-active.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: createModel.tableName,
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/order-active', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('order-active');

  service.hooks(hooks);
};
