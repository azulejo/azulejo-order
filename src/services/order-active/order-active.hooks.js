/* eslint-disable no-multiple-empty-lines */


const errors = require('@feathersjs/errors');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [
      (context) => {
        const orderQuery = {
          $limit: 1e2,
          status_status_id: {
            $in: [1, 7],
          },
        };

        const { user_id, contractor } = context.params.body;

        if (user_id) orderQuery.user_user_id = user_id;

        if (contractor) orderQuery.contractor = contractor;
        // search by contractor unless user_id is present

        if (contractor && user_id) orderQuery.user_user_id = null;
        // cleanup user field if contractor was passed (for all the new orders without user_id)

        if (!orderQuery.user_user_id && !orderQuery.contractor) throw new errors.BadRequest('Can not fetch order');

        return context.app.service('orders-order-data').find({ query: orderQuery })
          .then((maybeOrderData) => {
            if (!maybeOrderData.total) throw new errors.BadRequest('Can not fetch order');

            // eslint-disable-next-line no-param-reassign
            context.data.cost = maybeOrderData.data
              .reduce((acc, next) => acc + next.quantity * next.price, 0);

            return context.app.service('ordersorders').find({ query: orderQuery });
          })
          .then((maybeOrder) => {
            if (!maybeOrder.total) throw new errors.BadRequest('Can not fetch order');

            const [order] = maybeOrder.data;

            context.id = order.id; // eslint-disable-line no-param-reassign

            context.data.editedon = new Date(); // eslint-disable-line no-param-reassign

            // eslint-disable-next-line no-param-reassign
            context.data.user_user_id = context.params.body.user_id;

            // eslint-disable-next-line no-param-reassign
            if (!context.data.editedby) context.data.editedby = context.params.body.user_id;

            return context;
          });
      },
    ],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
