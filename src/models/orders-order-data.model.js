/* eslint-disable no-console */

// ordersOrderData-model.js - A KnexJS
//
// See http://knexjs.org/
// for more of what you can do here.
const TABLE_NAME = 'orders_orders';

module.exports = function OrdersOrdersDataModel (app) {
  const db = app.get('knexClient');
  const { prefix } = app.get('db');
  const tableName = `${prefix ? `${prefix}_` : ''}${TABLE_NAME}`;

  OrdersOrdersDataModel.tableName = tableName;

  db.schema.hasTable(tableName).then((exists) => {
    if(!exists) throw new Error(`\`${tableName}\` does not exist`);
  }).catch(console.log);


  return db;
};
