/* eslint-disable no-console */

// orders-model.js - A KnexJS
//
// See http://knexjs.org/
// for more of what you can do here.
const TABLE_NAME = 'orders_orders';

module.exports = function OrdersModel (app) {
  const db = app.get('knexClient');
  const { prefix } = app.get('db');
  const tableName = `${prefix ? `${prefix}_` : ''}${TABLE_NAME}`;

  OrdersModel.tableName = tableName;

  db.schema.hasTable(tableName).then((exists) => {
    if(!exists) throw new Error(`\`${tableName}\` does not exist`);
  }).catch(console.log);


  return db;
};
