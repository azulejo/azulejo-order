/* eslint-disable no-console */

// orderHistory-model.js - A KnexJS
//
// See http://knexjs.org/
// for more of what you can do here.
const TABLE_NAME = 'orders_order_history';

module.exports = function OrderHistoryModel (app) {
  const db = app.get('knexClient');
  const { prefix } = app.get('db');
  const tableName = `${prefix ? `${prefix}_` : ''}${TABLE_NAME}`;

  OrderHistoryModel.tableName = tableName;

  db.schema.hasTable(tableName).then((exists) => {
    if(!exists) throw new Error(`\`${tableName}\` does not exist`);
  });


  return db;
};
