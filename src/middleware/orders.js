const errors = require('@feathersjs/errors');
const debug = require('debug')('middleware:orders');
const { wait, forit } = require('a-wait-forit');

const orderSubmit = require('../middleware/order-submit');
const orderActive = require('../middleware/order-active');

module.exports = function ordersMiddleware () {
  const onError = (err) => { throw err; };

  const submit = orderSubmit({ onError });
  const active = orderActive({ onError });

  return wait(async (req, res, next) => {
    const { action } = req.params;

    debug('Got action', action);

    let result = [];

    if (action === 'submit') result = await forit(() => submit(req, res, next));
    if (action === 'active') result = await forit(() => active(req, res, next));

    const [requestErr, data] = result;

    if (requestErr && requestErr instanceof Error) return next(new errors.BadRequest(requestErr.message || 'Can not modify order'));

    if (!data) return next(new errors.NotFound('Can not modify order'));

    return res.send(data);
  });
};
