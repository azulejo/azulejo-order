const errors = require('@feathersjs/errors');
const { wait } = require('a-wait-forit');

const { validate, validator } = require('../validate/index');

// eslint-disable-next-line no-unused-vars
module.exports = function orderActiveMiddleware (options = {}) {
  return wait(async (req, res, next) => {
    const isValid = await validate(req.body, 'order-active');

    if (isValid.filter(_ => !_).length) return next(new errors.BadRequest(validator.errorsText()));
    // stop action if smth goes wrong

    const { app } = req;
    const { contractor, user_id } = req.body;

    const orderQuery = {
      status_status_id: {
        $in: [1, 7],
      },
    };

    if (user_id) {
      orderQuery.user_user_id = user_id;
    }

    if (contractor && !user_id) {
      orderQuery.contractor = contractor;
      // search by contractor unless user_id is present
    }

    return app.service('/orders-order-data').find({ query: orderQuery });
  }, options.onError);
};
