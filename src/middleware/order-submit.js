const errors = require('@feathersjs/errors');
const { wait, forit } = require('a-wait-forit');

const asym = require('../util/async-middleware');
const { validate, validator } = require('../validate/index');

// eslint-disable-next-line no-unused-vars
module.exports = function orderSubmitMiddleware (options = {}) {
  const _trx = app => new Promise((resolve) => {
    app.get('knexClient').transaction((transaction) => {
      resolve(transaction);
    });
  });

  return wait(async (req, res, next) => {
    const isValid = await validate(req.body, 'order-submit');

    if (isValid.filter(_ => !_).length) return next(new errors.BadRequest(validator.errorsText()));
    // stop action if smth goes wrong

    const {
      address,
      comment,
      shipping = '',
    } = req.body;

    const trx = await _trx(req.app);

    const maybeOrder = await req.app.service('order-active').patch(null, {
      address,
      comment,
      shipping,
      status_status_id: 2,
    }, {
      transaction: { trx },
      body: req.body,
    });

    if (!maybeOrder) return next(new errors.BadRequest('Can not submit order'));

    trx.commit();

    return Promise.resolve({
      data: {
        number: maybeOrder.number_id,
        cost: maybeOrder.cost,
      },
    });
  }, options.onError);
};
