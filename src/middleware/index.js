const upload = require('multer')();

const { addSchemas } = require('../validate');

const products = require('./products');

const orders = require('./orders');

module.exports = function middleware (app) {
  addSchemas(app);

  app.use('*', upload.array());

  app.use('/products/:action', products());

  app.use('/orders/:action', orders());
};
