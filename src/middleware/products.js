const cond = require('ramda/src/cond');
const errors = require('@feathersjs/errors');
const debug = require('debug')('middleware:products');

const { validate } = require('../validate');
const asym = require('../util/async-middleware');

const _trx = app => new Promise((resolve) => {
  app.get('knexClient').transaction((transaction) => {
    resolve(transaction);
  });
});

const changeProductOnAnonymusOrder = async (req, res, next) => {
  const isValid = await validate(req.body, 'product-add', 'anonymus-order');

  if (isValid.filter(_ => !_).length) return next(new errors.BadRequest());
  // stop action if smth goes wrong

  const { app } = req;

  if (typeof req.body.quantity === 'undefined') req.body.quantity = 1;

  const {
    contractor,
    currency_currency_id,
    price_old,
    price,
    product_id,
    quantity,
  } = req.body;

  const trx = await _trx(app);

  const orderQuery = {
    contractor, $limit: 1, status_status_id: 1, user_user_id: null,
  };

  const maybeOrder = await app.service('/ordersorders').find({ query: orderQuery });
  // check available order

  let [order] = maybeOrder.data;
  let existedProduct;

  if (!order) {
    const createOrderQuery = { contractor };

    debug('Create new order', createOrderQuery);
    order = await app.service('/ordersorders').create(createOrderQuery, { transaction: { trx } });
    // create order unless was found
  } else {
    const productQuery = {
      query: {
        product_id,
        order_order_id: order.id,
        $limit: 1,
      },
    };

    debug('Maybe product exists', productQuery);
    const maybeProductExisted = await app.service('/products').find(productQuery);
    // order does exist. should check if product was already added to order

    [existedProduct] = maybeProductExisted.data;
  }

  const nextQuantity = existedProduct ? (
    quantity === 0
      ? 0
      : existedProduct.quantity + quantity
  ) : quantity;

  const nextProduct = existedProduct || {
    currency_currency_id,
    order_order_id: order.id,
    price_old,
    price,
    product_id,
  };

  const nextProductQuery = Object
    .assign({}, nextProduct, { quantity: nextQuantity <= 0 ? 0 : nextQuantity });

  debug(`${!existedProduct ? 'Create' : 'Update'} order's product`, nextProductQuery);

  return !existedProduct
    ? app.service('/products').create(nextProductQuery, { transaction: { trx } })
    : app.service('/products').update(nextProductQuery.id, nextProductQuery, { transaction: { trx } });
};

const changeProductOnRealOrder = async (req, res, next) => {
  const isValid = await validate(req.body, 'product-add', 'order');

  if (isValid.filter(_ => !_).length) return next(new errors.BadRequest());
  // stop action if smth goes wrong

  const { app } = req;
  const { order_id, user_id } = req.body;

  const orderQuery = {
    id: order_id, user_user_id: user_id, $limit: 1, status_status_id: 1,
  };

  debug('Query order', orderQuery);

  const [maybeOrder] = await app.service('/ordersorders').find({ query: orderQuery });
  // check available order

  if (!maybeOrder) return next(new errors.BadRequest());

  const {
    product_id, quantity, price_old, price, currency_currency_id,
  } = req.body;

  const currentProductQuery = {
    query: {
      product_id,
      order_order_id: maybeOrder.id,
      $limit: 1,
    },
  };

  debug('Query product', currentProductQuery);

  const [maybeExistedProduct] = await app.service('/products').find(currentProductQuery);
  // lookup for product in order

  let nextQuantity = maybeExistedProduct ? maybeExistedProduct.quantity + quantity : quantity;

  nextQuantity = nextQuantity <= 0 ? 0 : nextQuantity;

  const nextProduct = maybeExistedProduct || {
    currency_currency_id,
    order_order_id: maybeOrder.id,
    price_old,
    price,
    product_id,
  };

  const nextProductQuery = Object.assign({}, nextProduct, { quantity: nextQuantity });

  debug('Query next product', nextProductQuery);

  return !maybeExistedProduct
    ? app.service('/products').create(nextProductQuery)
    : app.service('/products').update(nextProductQuery.id, nextProductQuery);
};

const add = (req, res, next) => {
  const {
    contractor, order_id, user_id,
  } = req.body;

  if (order_id && user_id) return changeProductOnRealOrder(req, res, next);
  // try to edit real order if order_id was passed

  if (contractor) return changeProductOnAnonymusOrder(req, res, next);
  // try to edit possible order if contractor was passed

  return next(new errors.BadRequest());
};

const edit = (req, res, next) => debug('Not implemented') || next();

module.exports = function productsMiddleware () {
  return asym(async (req, res, next) => {
    const { action } = req.params;

    const maybeAction = cond([
      [_ => _ === 'add', () => add(req, res, next)], // eslint-disable-line array-element-newline
      [_ => _ === 'edit', () => edit(req, res, next)], // eslint-disable-line array-element-newline
      [() => true, () => next(new errors.NotFound('Page not found'))],
    ]);

    const result = await maybeAction(action);

    return res.send(result || { message: 'Can not update products' });
  });
};
