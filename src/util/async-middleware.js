module.exports = (fn, callback, proceed = 2) => (...argv) => {
  // expects middleware vector by default
  // `proceed` allows to redefine position of next/reject function
  try {
    const maybePromise = fn(...argv);

    if (maybePromise instanceof Promise) return maybePromise.catch(argv[proceed]);

    if (typeof callback === 'function') callback(null, maybePromise);

    return maybePromise;
  } catch (error) {
    if (argv[2]) return argv[2](error);

    if (typeof callback === 'function') return callback(error);

    throw error instanceof Error ? error : new Error(error || 'asym error');
  }
};
