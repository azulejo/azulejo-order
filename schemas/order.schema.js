exports.orderAnonymus = {
  title: 'anonymus-order',
  type: 'object',
  required: ['contractor'],
  properties: {
    contractor: {
      type: 'string',
    },
  },
};

exports.order = {
  title: 'order',
  type: 'object',
  required: ['order_id', 'user_id'],
  properties: {
    order_id: {
      type: 'number',
    },
    user_id: {
      type: 'number',
    },
  },
};

exports.orderActive = {
  title: 'order-active',
  type: 'object',
  required: ['contractor', 'user_id'],
  properties: {
    user_id: {
      type: 'number',
    },
    contractor: {
      type: 'string',
    },
  },
};

exports.orderSubmit = {
  title: 'order-submit',
  type: 'object',
  required: ['address', 'email', 'fullname'],
  properties: {
    fullname: {
      type: 'string',
    },
    email: {
      type: 'string',
      format: 'email',
      message: 'Please enter valid email',
      description: 'Age in years',
      errorMessage: {
        format: 'string',
        message: 'Invalid email',
      },
    },
    shipping: {
      type: 'number',
    },
    phone: {
      type: 'string',
    },
    address: {
      type: 'string',
    },
    comment: {
      type: 'string',
    },
    shipping_cost: {
      type: 'number',
    },
    cost: {
      type: 'number',
    },
  },
};

exports.productAdd = {
  title: 'product-add',
  type: 'object',
  required: ['product_id', 'currency_currency_id', 'price'],
  properties: {
    product_id: {
      type: 'number',
    },
    currency_currency_id: {
      type: 'number',
    },
    quantity: {
      type: 'number',
    },
    price: {
      type: 'number',
    },
    price_old: {
      type: 'number',
    },
  },
};
