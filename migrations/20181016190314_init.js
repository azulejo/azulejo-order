const debug = require('debug')('migrations:init');

const app = require('../index');
const asym = require('../lib/util/async-middleware');

const { prefix } = app.get('db');

const wrapAsync = fn => asym(fn, null, 1);

const tableName = name => `${prefix ? `${prefix}_` : ''}${name}`;

exports.up = function Up (knex, Promise) {
  const statusesTable = tableName('orders_statuses');
  const ordersTable = tableName('orders_orders');
  const productsTable = tableName('orders_order_products');
  const orderHistoryTable = tableName('orders_order_history');

  return new Promise(wrapAsync(async (resolve, reject) => { // eslint-disable-line no-unused-vars
    debug(`Run migration on \`${statusesTable}\``);
    const hasStatusesTable = await knex.schema.hasTable(statusesTable);

    if (!hasStatusesTable) {
      debug(`\`${statusesTable}\` does not exist. Create new one`);
      await knex.schema.createTable(statusesTable, (table) => {
        table.increments('id');

        table.string('name', 25).notNullable();
        table.string('color', 10);
        table.integer('rank', 4).defaultTo(0).unsigned();
        table.integer('comment', 100);

        table.unique('name', 'name');
      });
    } else { debug(`\`${statusesTable}\` exists`); }

    debug(`Run migration on \`${ordersTable}\``);
    const hasOrdersTable = await knex.schema.hasTable(ordersTable);

    if (!hasOrdersTable) {
      debug(`\`${ordersTable}\` does not exist. Create new one`);
      await knex.schema.createTable(ordersTable, (table) => {
        table.increments('id');

        table.integer('history_id', 10).unsigned().notNullable();
        table.integer('status_status_id', 10).unsigned().notNullable();
        table.integer('user_user_id', 10).unsigned();
        table.integer('number_id', 10).unsigned().notNullable();

        table.float('cost').unsigned();
        table.float('discount', 3, 2).unsigned();
        table.float('shipping_cost').unsigned();

        table.string('contractor').notNullable();

        table.string('shipping', 20);
        table.string('address');
        table.string('comment');

        table.integer('createdby', 10).unsigned();
        table.integer('editedby', 10).unsigned();
        table.timestamp('createdon').defaultTo(knex.fn.now());
        table.timestamp('editedon');

        table.unique('number_id', 'number_id');
        table.index('history_id', 'history_id');
        table.index('status_status_id', 'status_id');
        table.index('user_user_id', 'user_id');
        table.index('contractor', 'contractor');
        table.index('createdby', 'createdby');
        table.index('editedby', 'editedby');

        table.foreign('status_status_id').references(`${statusesTable}.id`).onDelete('restrict');
      });
    } else { debug(`\`${ordersTable}\` exists`); }

    debug(`Run migration on \`${orderHistoryTable}\``);
    const hasOrderHistoryTable = await knex.schema.hasTable(orderHistoryTable);

    if (!hasOrderHistoryTable) {
      debug(`\`${orderHistoryTable}\` does not exist. Create new one`);
      await knex.schema.createTable(orderHistoryTable, (table) => {
        table.increments('id');

        table.integer('order_order_id', 10).unsigned().notNullable();
        table.integer('history_id', 10).unsigned().notNullable();
        table.integer('status_status_id', 10).unsigned().notNullable();
        table.integer('user_user_id', 10).unsigned();
        table.integer('number_id', 10).unsigned().notNullable();

        table.float('cost').unsigned();
        table.float('discount', 3, 2).unsigned();
        table.float('shipping_cost').unsigned();

        table.string('contractor').notNullable();

        table.string('shipping', 20);
        table.string('address');
        table.string('comment');

        table.integer('createdby', 10).unsigned();
        table.integer('editedby', 10).unsigned();
        table.timestamp('createdon').defaultTo(knex.fn.now());
        table.timestamp('editedon');

        table.index('number_id', 'number_id');
        table.index('order_order_id', 'order_order_id');
        table.index('history_id', 'history_id');
        table.index('status_status_id', 'status_id');
        table.index('user_user_id', 'user_id');
        table.index('contractor', 'contractor');
        table.index('createdby', 'createdby');
        table.index('editedby', 'editedby');

        table.foreign('status_status_id').references(`${statusesTable}.id`).onDelete('restrict');
        table.foreign('order_order_id').references(`${ordersTable}.id`).onDelete('cascade');
      });
    } else { debug(`\`${orderHistoryTable}\` exists`); }

    debug(`Run migration on \`${productsTable}\``);
    const hasProductsTable = await knex.schema.hasTable(productsTable);

    if (!hasProductsTable) {
      debug(`\`${productsTable}\` does not exist. Create new one`);
      await knex.schema.createTable(productsTable, (table) => {
        table.increments('id');
        table.integer('order_order_id', 10).unsigned().notNullable();
        table.integer('product_id', 10).unsigned().notNullable();
        table.integer('quantity').defaultTo(0).unsigned().notNullable();
        table.float('price').defaultTo(0).unsigned().notNullable();
        table.float('price_old').defaultTo(0).unsigned().notNullable();
        table.integer('currency_currency_id', 10).unsigned().notNullable();

        table.unique(['order_order_id', 'product_id'], 'orderproduct_id');
        table.index('currency_currency_id', 'currency_id');
        table.index('price', 'price');
        table.index('quantity', 'quantity');

        table.foreign('order_order_id').references(`${ordersTable}.id`).onDelete('cascade');
      });
    } else { debug(`\`${productsTable}\` exists`); }

    resolve('Migration was complete');
  }));
};

exports.down = function Down (knex, Promise) {
  return Promise.resolve('Down is not implemented');
};
