const debug = require('debug')('application');

debug('NODE_ENV', process.env.NODE_ENV);
module.exports = require('./lib');
